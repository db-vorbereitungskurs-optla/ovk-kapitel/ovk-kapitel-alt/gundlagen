const cssnano = require('cssnano');
const { merge } = require('webpack-merge');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const CssMinimizerPlugin = require("css-minimizer-webpack-plugin");
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const common = require('./webpack.config.common.js');
var webpack = require('webpack');

const production_domain = 'https://gundlagen-db-vorbereitungskurs-optla-ovk-kapitel-6233b8a688bd0b.gitlab.io'

module.exports = merge(common, {
    mode: 'production',
    optimization: {
        minimize: true,
        minimizer: [
            new CssMinimizerPlugin(),
        ],
    },
    module: {
        rules: [{
            test: /\.(sass|scss)$/,
            use: [
                // Inserts all imported styles into the html document
                { loader: "style-loader" },

                // Translates CSS into CommonJS
                { loader: "css-loader" },

                // Compiles Sass to CSS
                {
                    loader: "sass-loader"
                },
                // Reads Sass vars from files or inlined in the options property
                {
                    loader: "@epegzz/sass-vars-loader",
                    options: {
                        syntax: 'scss',
                        // Option 1) Specify vars here
                        vars: {
                            domainRoot: JSON.stringify(production_domain)
                        }
                    }
                }
            ]
        }, ],
    },
    plugins: [
        new CleanWebpackPlugin(),
        new webpack.DefinePlugin({
            __DOMAIN_ROOT__: JSON.stringify(production_domain),
        }),
        new MiniCssExtractPlugin({
            filename: '[name].css',
            chunkFilename: '[id].css',
        })
    ],
});
